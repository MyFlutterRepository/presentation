import 'package:flutter/material.dart';
import 'package:presentation_app/src/content/home_content.dart';
import 'package:todo_list_app/todo_list.dart';

import 'src/content/todo_list_content.dart';
import 'src/presentation_page.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Flutter Portfolio',
      initialRoute: '/',
      routes: {
        '/': (context) => const PresentationPage($HomeContent),
        '/todo_list': (context) => const PresentationPage($TodoList),
        'app/todo_list': (context) => const TodoList(),
      },
    ),
  );
}
