import 'package:flutter/material.dart';
import 'package:presentation_app/src/tokens/theme.dart';

class LanguageButtonWidget extends StatelessWidget {
  final void Function(bool isPtBr) onPressed;
  final ValueNotifier<bool> _isPtBr = ValueNotifier(true);

  LanguageButtonWidget({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  void onChange() {
    _isPtBr.value = !_isPtBr.value;
    onPressed(_isPtBr.value);
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: _isPtBr,
      builder: (context, isPtBr, _) {
        return Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: !isPtBr ? onChange : null,
                  child: Text(
                    'Pt-Br',
                    style: TextStyle(
                      color: isPtBr
                          ? AppTheme.i.color.blue
                          : AppTheme.i.color.text1,
                    ),
                  ),
                ),
                InkWell(
                  onTap: isPtBr ? onChange : null,
                  child: Text(
                    'En-Us',
                    style: TextStyle(
                      color: !isPtBr
                          ? AppTheme.i.color.blue
                          : AppTheme.i.color.text1,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
