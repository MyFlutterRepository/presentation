import 'package:flutter/material.dart';

import '../tokens/font_styles.dart';
import '../tokens/theme.dart';

class ContentListTile extends StatelessWidget {
  final String title;
  final String route;
  const ContentListTile({
    Key? key,
    required this.title,
    required this.route,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          ListTile(
            title: Text(
              title,
              style: FontStyles.body2SemiBoldText1,
            ),
            onTap: () => Navigator.of(context).pushReplacementNamed(route),
            trailing: SizedBox.square(
              dimension: 16,
              child: Icon(
                Icons.arrow_forward_ios_outlined,
                color: AppTheme.i.color.text1,
              ),
            ),
          ),
          Divider(
            color: AppTheme.i.color.text1,
          ),
        ],
      ),
    );
  }
}
