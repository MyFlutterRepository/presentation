import 'package:flutter/material.dart';
import 'package:presentation_app/src/tokens/theme.dart';

class ThemeModeButtonWidget extends StatelessWidget {
  final void Function(bool isDarkMode) onPressed;
  final ValueNotifier<bool> _isDarkMode;

  ThemeModeButtonWidget({
    Key? key,
    required this.onPressed,
    required bool isDarkMode,
  })  : _isDarkMode = ValueNotifier(isDarkMode),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: _isDarkMode,
      builder: (context, isDarkMode, _) {
        return IconButton(
          onPressed: () {
            _isDarkMode.value = !_isDarkMode.value;
            onPressed(_isDarkMode.value);
          },
          icon: isDarkMode
              ? Icon(
                  Icons.dark_mode,
                  color: AppTheme.i.color.text1,
                )
              : Icon(
                  Icons.light_mode,
                  color: AppTheme.i.color.yellow,
                ),
        );
      },
    );
  }
}
