import 'package:flutter/material.dart';

import '../tokens/font_styles.dart';
import '../tokens/theme.dart';
import 'content_list_tile.dart';

class TopicsWidget extends StatelessWidget {
  const TopicsWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Text(
            'Contents',
            style: FontStyles.headline2RegularBlueEricaOne,
          ),
          Divider(
            color: AppTheme.i.color.text1,
          ),
          Expanded(
            child: ListView(
              children: const [
                ContentListTile(
                  title: 'Home',
                  route: '/',
                ),
                ContentListTile(
                  title: 'Todo List',
                  route: '/todo_list',
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
