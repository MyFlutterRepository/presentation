import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:presentation_app/src/tokens/font_styles.dart';
import 'package:presentation_app/src/tokens/theme.dart';
import 'package:html_stub/html_stub.dart' as html if (dart.library.html) 'dart:html';

class FooterWidget extends StatelessWidget {
  const FooterWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: AppTheme.i.updateTheme,
      builder: (context, _, __) {
        return SizedBox(
          height: 60,
          child: Column(
            children: [
              Divider(
                color: AppTheme.i.color.blue,
                thickness: 2,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Marcos Willian de Sales Reis',
                            style: FontStyles.body2RegularText2,
                          ),
                          if (kIsWeb)
                            InkWell(
                              onTap: () =>  html.window.open(
                                  'https://www.google.com/search?q=flutter+open+link&oq=flutter+open+link+&aqs=edge..69i57j0i512l4j0i22i30l3j69i64.6813j0j4&sourceid=chrome&ie=UTF-8',
                                  'Repository'),
                              child: Text(
                                'Root Directory',
                                style: FontStyles.body1RegularText2Underlined,
                              ),
                            ),
                        ],
                      ),
                    ),
                    Text(
                      '2023',
                      style: FontStyles.body2RegularText2,
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
