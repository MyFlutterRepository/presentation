import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'tokens/font_styles.dart';

import 'tokens/theme.dart';
import 'widgets/footer_widget.dart';
import 'widgets/language_button_widget.dart';
import 'widgets/theme_mode_button_widget.dart';
import 'widgets/topics_widget.dart';

class PresentationPage extends StatelessWidget {
  final String content;
  const PresentationPage(this.content, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool showDrawer = !kIsWeb || MediaQuery.of(context).size.width < 1000;
    return ValueListenableBuilder(
      valueListenable: AppTheme.i.updateTheme,
      builder: (context, __, ___) => Scaffold(
        drawer: showDrawer
            ? Drawer(
                child: Container(
                  color: AppTheme.i.color.background,
                  height: double.maxFinite,
                  width: double.maxFinite,
                  child: const TopicsWidget(),
                ),
              )
            : null,
        backgroundColor: AppTheme.i.color.scaffoldBackground,
        appBar: AppBar(
          foregroundColor: AppTheme.i.color.text1,
          shadowColor: AppTheme.i.color.text1,
          centerTitle: kIsWeb,
          backgroundColor: AppTheme.i.color.background,
          actions: [
            ThemeModeButtonWidget(
              isDarkMode: AppTheme.i.themeMode == ThemeMode.dark,
              onPressed: (isDarkMode) {
                AppTheme.i.themeMode =
                    isDarkMode ? ThemeMode.dark : ThemeMode.light;
              },
            ),
            LanguageButtonWidget(onPressed: (isPtBr) async {}),
          ],
          title: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Portfolio Flutter',
                  style: kIsWeb
                      ? FontStyles.headline2RegularBlueEricaOne
                      : FontStyles.headline1BoldBlue,
                ),
                TextSpan(
                  text: '${kIsWeb ? '' : '\n'} - by Marcos Reis',
                  style: FontStyles.body2SemiBoldText1Italic,
                ),
              ],
            ),
          ),
        ),
        body: SafeArea(
          child: LayoutBuilder(builder: (context, constraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(minHeight: constraints.maxHeight),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (!showDrawer) ...[
                          SizedBox(
                            width: 300,
                            height: constraints.maxHeight - 60,
                            child: const Padding(
                              padding: EdgeInsets.only(top: 8.0),
                              child: TopicsWidget(),
                            ),
                          ),
                          SizedBox(
                            height: constraints.maxHeight - 60,
                            child: VerticalDivider(
                              color: AppTheme.i.color.blue,
                              thickness: 2,
                            ),
                          ),
                        ],
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(32),
                            child: MarkdownBody(
                              data: content,
                              styleSheet: FontStyles.makrdownStyleSheet,
                              onTapLink: (_, app, __) {
                                Navigator.pushReplacementNamed(
                                  context,
                                  app ?? '/',
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    const FooterWidget(),
                  ],
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
