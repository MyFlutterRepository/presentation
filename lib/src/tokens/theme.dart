import 'package:flutter/material.dart';

abstract class AppColors {
  Color get background;
  Color get blue;
  Color get text1;
  Color get text2;
  Color get yellow;
  Color get scaffoldBackground;
}

abstract class GlobalColors implements AppColors {
  @override
  Color get blue => Colors.blue.shade400;

  @override
  Color get yellow => Colors.yellow.shade400;
}

class LightColors extends GlobalColors {
  LightColors._();

  static final _instance = LightColors._();

  static AppColors get i => _instance;

  @override
  get background => const Color(0xFFFFFFFF);

  @override
  Color get text1 => Colors.black87;

  @override
  Color get scaffoldBackground => Colors.white;

  @override
  Color get text2 => Colors.black;
}

class DarkColors extends GlobalColors {
  DarkColors._();

  static final _instance = DarkColors._();

  static AppColors get i => _instance;

  @override
  get background => Colors.black54;

  @override
  Color get text1 => Colors.white70;

  @override
  Color get scaffoldBackground => Colors.black87;

  @override
  Color get text2 => Colors.white;
}

class AppTheme {
  ThemeMode _themeMode;

  ValueNotifier updateTheme = ValueNotifier(null);
  ThemeMode get themeMode => _themeMode;
  set themeMode(ThemeMode themeMode) {
    _themeMode = themeMode;
    // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
    updateTheme.notifyListeners();
  }

  AppTheme._() : _themeMode = ThemeMode.dark;

  static final _instance = AppTheme._();

  static AppTheme get i => _instance;

  AppColors get color {
    if (_themeMode == ThemeMode.dark) {
      return DarkColors.i;
    } else {
      return LightColors.i;
    }
  }
}
