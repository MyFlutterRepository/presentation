import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

import 'theme.dart';

class FontStyles {
  static get headline2RegularBlueEricaOne => TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w400,
        fontFamily: 'EricaOne',
        color: AppTheme.i.color.blue,
      );
  static get headline1BoldBlue => TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.w800,
        color: AppTheme.i.color.blue,
      );

  static get body2SemiBoldText1Italic => TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.italic,
        color: AppTheme.i.color.text1,
      );

  static get body2SemiBoldText1 => TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
        color: AppTheme.i.color.text1,
      );

  static get body1SemiBoldText1 => TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w500,
        color: AppTheme.i.color.text1,
      );

  static get body2RegularText2 => TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w400,
        color: AppTheme.i.color.text2,
      );

  static get body1RegularText2Underlined => TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: AppTheme.i.color.text2,
        decoration: TextDecoration.underline,
      );

  static get makrdownStyleSheet => MarkdownStyleSheet(
        h1: FontStyles.headline1BoldBlue,
        p: FontStyles.body1SemiBoldText1,
        checkbox: FontStyles.body2RegularText2,
        listBullet: FontStyles.body2RegularText2,
      );
}
